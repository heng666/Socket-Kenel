# Socket Kenel

### 介绍
自制的 C# Socket 常用通讯内核

实现了基本的高并发收发以及心跳功能，满足日常的通信要求。

### 使用说明

Socket Kenel\Helper\Socket 文件夹内为可复用通信方法
1. SocketClient_Kenel 客户端内核（集成了心跳监测）
2. SocketServer_Kenel 服务器端内核
3. SocketConnection 服务器端每次监听到的连接


### 如果我是写Socket客户端
#### 初始化

- 只需要引用 SocketClient_Kenel.cs
```
//创建一个实例，并给与她一个IP和端口号
SocketClient_Kenel s_client= new SocketClient_Kenel(new []{"127.0.0.1","5000"});

//并初始化她，此时她才真正开始工作。所以可以利用这一点，在你想让她工作的时候在调用。
 s_client.Init();
```

#### 如何接受数据

```
 
 //在一个常驻线程中，轮询这个接受队列即可。
 Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    Thread.Sleep(1);
                    if (!s_client.q_rec.IsEmpty) //队列不为空则取出
                    {
                        s_client.q_rec.TryDequeue(out byte[] data);
                        //这里可以对data进行自定义协议的解析 或者像下面这样直接转成字符串 
                        string res = Encoding.UTF8.GetString(data);
                        if (!string.IsNullOrWhiteSpace(res))
                        { tb_send.Invoke(new Action(() => { tb_send.Text = res; })); }
                    }
                }
            }, TaskCreationOptions.LongRunning);
```
#### 如何发送数据

```
 //一句话搞定
 s_client.Send("我是被发送的数据~");
```


### 如果我是写服务器端的
#### 初始化
- 需要引用SocketServer_Kenel.cs 和SocketConnection.cs
```
//初始化一个服务器端实例，也给她一个IP和端口号
public SocketServer_Kenel s_srv = new SocketServer_Kenel("127.0.0.1", 9876);
//她什么时候开始工作，你来决定
s_srv.StartServer();
```

#### 如何接受数据

```
 //每接收到一个新的连接时候触发
            s_srv.HandleNewClientConnected += new Action<SocketServer_Kenel, SocketConnection>((ss, sc) =>
            {
                //为每一个连接上来的客户端建立一个读取线程
                Task.Factory.StartNew(() =>
                {
                    while (true)
                    {
                        try
                        {
                            if (!sc.q_rec.IsEmpty)
                            {
                                sc.q_rec.TryDequeue(out byte[] data);
                                //这里可以对data进行自定义协议的解析 或者像下面这样直接转成字符串 
                                string res = Encoding.UTF8.GetString(data);
                                if (!string.IsNullOrWhiteSpace(res))
                                { tb_send.Invoke(new Action(() => { tb_send.Text = res; })); }
                            }
                        }
                        catch { break; }

                    }
                });
            });
```
#### 如何发送数据（给所有连接都发送）

```
  //获取所有已经连接上的实例
  List<SocketConnection> list = s_srv.GetConnectionList().ToList();
  //给每一个连接发送这个消息
  foreach (SocketConnection item in list)
  {
      item.Send(tb_send.Text);
  }
```
