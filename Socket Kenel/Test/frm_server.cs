﻿using Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test
{
    public partial class frm_server : Form
    {
        public frm_server()
        {
            InitializeComponent();
            Text = "服务器端";
            init_ctls();
            init_socket();
        }

        #region 界面控件
        TextBox tb_send = new TextBox(); //输入框
        public void init_ctls()
        {
            StartPosition = FormStartPosition.CenterScreen;
            Width = 440;
            Height = 500;

            Button btn_new_client = new Button();
            btn_new_client.Location = new Point(10, 10);
            btn_new_client.Width = 160;
            btn_new_client.Text = "召唤一个客户端";
            btn_new_client.Click += Btn_new_client_Click;
            Controls.Add(btn_new_client);


            tb_send.Name = "txt_send";
            tb_send.Location = new Point(10, btn_new_client.Height + btn_new_client.Top + 10);
            tb_send.Multiline = true;
            tb_send.Height = 200;
            tb_send.Width = 400;
            Controls.Add(tb_send);

            Button btn_send = new Button();
            btn_send.Location = new Point(10, tb_send.Top + tb_send.Height + 10);
            btn_send.Text = "发送";
            btn_send.Click += Btn_send_Click;
            Controls.Add(btn_send);
        }
        /// <summary>
        /// 建一个新的客户端
        /// 自动连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Btn_new_client_Click(object sender, EventArgs e)
        {
            frm_client frm = new frm_client();
            frm.Show();
        }
        #endregion

        public SocketServer_Kenel s_srv = new SocketServer_Kenel("127.0.0.1", 9876);
        /// <summary>
        /// 初始化
        /// </summary>
        public void init_socket()
        {
            s_srv.StartServer();
            //每接收到一个新的连接时候触发
            s_srv.HandleNewClientConnected += new Action<SocketServer_Kenel, SocketConnection>((ss, sc) =>
            {
                //为每一个连接上来的客户端建立一个读取线程
                Task.Factory.StartNew(() =>
                {
                    while (true)
                    {
                        try
                        {
                            if (!sc.q_rec.IsEmpty)
                            {
                                sc.q_rec.TryDequeue(out byte[] data);
                                //这里可以对data进行自定义协议的解析 或者像下面这样直接转成字符串 
                                string res = Encoding.UTF8.GetString(data);
                                if (!string.IsNullOrWhiteSpace(res))
                                { tb_send.Invoke(new Action(() => { tb_send.Text = res; })); }
                            }
                        }
                        catch { break; }

                    }
                });
            });
        }

        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Btn_send_Click(object sender, EventArgs e)
        {
            //获取所有已经连接上的实例
            List<SocketConnection> list = s_srv.GetConnectionList().ToList();
            //给每一个连接发送这个消息
            foreach (SocketConnection item in list)
            {
                item.Send(tb_send.Text);
            }
        }
    }
}
